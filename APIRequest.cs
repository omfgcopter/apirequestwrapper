using BestHTTP;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serv.Data.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

namespace SirinInteractive.FinGame.API
{
    public class APIRequest
    {
        public HTTPRequest httpRequest { get; private set; }
        public HTTPResponse httpResponse { get; private set; }

        public Subject<APIResponse> OnSuccess = new Subject<APIResponse>();
        public Subject<APIResponse> OnError = new Subject<APIResponse>();
        public Subject<APIResponse> OnAuthError = new Subject<APIResponse>();
        public Subject<APIResponse> OnNoResponse = new Subject<APIResponse>();


        public JToken Json { get; private set; }
        public Response ParsedResponse { get; private set; }
        public string Path { get; private set; }
        public EResponseState responseState { get; private set; }


        public static APIRequest Post(string path) { return new APIRequest(HTTPMethods.Post, path); }

        public static APIRequest Put(string path) { return new APIRequest(HTTPMethods.Put, path); }

        public static APIRequest Put(string path, string token) { return new APIRequest(HTTPMethods.Put, path, token); }

        public static APIRequest Post(string path, string token) { return new APIRequest(HTTPMethods.Post, path, token); }

        public static APIRequest Get(string path) { return new APIRequest(HTTPMethods.Get, path); }

        public static APIRequest Get(string path, string token) { return new APIRequest(HTTPMethods.Get, path, token); }

        private int tryCounts;
        private int maxRetryCount = 3;
        private TaskCompletionSource<HTTPResponse> tcs;

        public APIRequest(HTTPMethods method, string path, string token) : this(method, path)
        {
            AddAuthorization(token);
        }

        public APIRequest(HTTPMethods method, string path)
        {
            Path = path;

            HTTPRequest request = new HTTPRequest(new Uri(path), method, OnRequestFinished);
            request.Timeout = TimeSpan.FromSeconds(RequestProps.defaultRequestTimeout);
            request.ConnectTimeout = TimeSpan.FromSeconds(RequestProps.defaultRequestConnectTimeout);

            httpRequest = request;

            AddHeaders();

            tryCounts = 0;
        }

        public TimeSpan Timeout
        {
            get => httpRequest.Timeout; set => httpRequest.Timeout = value;
        }

        public TimeSpan ConnectTimeout
        {
            get => httpRequest.ConnectTimeout; set => httpRequest.ConnectTimeout = value;
        }

        public byte[] RawData
        {
            get => httpRequest.RawData; set => httpRequest.RawData = value;
        }

        private void OnRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
        {
            this.httpRequest = originalRequest;
            this.httpResponse = response;
            if (response == null)
            {
                Resend();
                return;
            }

            Debug.Log($"{Path} - {response.DataAsText}");
            Json = JToken.Parse(response.DataAsText);

            ParsedResponse = JsonConvert.DeserializeObject<Response>(Json["response"].ToString());

            if (response.StatusCode == (int)HttpStatusCode.OK)
            {
                if (ParsedResponse.IsSuccsesful)
                {
                    OnSuccess.OnNext(new APIResponse(originalRequest, response));
                }
                else
                {
                    OnError.OnNext(new APIResponse(originalRequest, response));
                }
            }
            else
            {
                if (response.StatusCode == (int)HttpStatusCode.Unauthorized)
                {
                    OnAuthError.OnNext(null);
                }
                else
                {
                    OnError.OnNext(new APIResponse(originalRequest, response));
                }
            }
        }

        private void Resend()
        {
            tryCounts++;
            if (tryCounts > maxRetryCount)
            {
                Send();
            }
            else
            {
                OnNoResponse.OnNext(null);
            }
        }

        public void Send()
        {
            Debug.Log($"<color=#00CC55>Sending request to {Path}</color>");
            httpRequest.Send();
        }

        public async Task<HTTPResponse> SendAsync()
        {
            try
            {
                Debug.Log($"<color=#00CC55>Sending request to {Path}</color>");

                tcs = new TaskCompletionSource<HTTPResponse>();

                httpRequest.Callback += HandleAsyncResponse;
                httpRequest.Send();

                httpResponse = await tcs.Task;

                return httpResponse;
            }
             catch (Exception e)
            {
                throw e;
            }
        }

        private void HandleAsyncResponse(HTTPRequest req, HTTPResponse resp)
        {
            if (req.Exception != null)
            {
                responseState = EResponseState.error;
                Debug.LogError($"Request failed: {req.Exception}");
                tcs.TrySetException(req.Exception);
                OnError.OnNext(new APIResponse(req, resp));
            }
            else if (resp == null)
            {
                tryCounts++;
                if (tryCounts > maxRetryCount)
                {
                    Send();
                }
                else
                {
                    responseState = EResponseState.noResponse;
                    Debug.LogError($"No response for call to: {Path}");
                    tcs.TrySetResult(resp);
                    OnNoResponse.OnNext(new APIResponse(req, resp));
                }
            }
            else if (resp.StatusCode >= 400)
            {
                responseState = EResponseState.error;
                Debug.LogError($"Request failed with status code {resp.StatusCode}: {resp.Message}");
                tcs.TrySetException(new HttpRequestException($"Request failed with status code {resp.StatusCode}: {resp.Message}"));
                OnError.OnNext(new APIResponse(req, resp));
            }
            else if (resp.StatusCode == (int)HttpStatusCode.Unauthorized)
            {
                responseState = EResponseState.authError;
                Debug.LogError($"Failed to authorize request.");
                OnAuthError.OnNext(new APIResponse(req, resp));
            }
            else
            {
                responseState = EResponseState.succes;
                Debug.Log($"Request succeeded: {resp.Message}");
                tcs.TrySetResult(resp);
                OnSuccess.OnNext(new APIResponse(req, resp));
            }
        }

        public async Task<JToken> SendAsyncReturnJson()
        {
            try
            {
                var response = await SendAsync();
                return JToken.Parse(response.DataAsText);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return null;
            }
        }

        public async Task<T> SendAsync<T>()
        {
            try
            {
                var response = await SendAsync();
                var result = JsonConvert.DeserializeObject<T>(response.DataAsText);
                return result;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                throw e;
            }
        }


        public void AddHeaders()
        {
            httpRequest.AddHeader("Accept", "application/json");
            httpRequest.AddHeader("Content-Type", "application/json");
        }

        public void AddAuthorization(string token)
        {
            httpRequest.AddHeader("Authorization", $"Bearer {token}");
        }
    }

    public class APIResponse
    {
        public HTTPRequest request;
        public HTTPResponse response;

        public APIResponse(HTTPRequest request, HTTPResponse response)
        {
            this.request = request;
            this.response = response;
        }
    }


    public enum EResponseState
    {
        succes, error, authError, noResponse
    }
}