# API Request Wrapper

Solution to simplify and unify API web calls to a web server, that return JSON objects. 

## Examples

### Example 1, Callbacks:

```
var request = APIRequest.Get("URL");

request.OnNoResponse.Subscribe(NoResponseHandler).AddTo(disposables);
request.OnError.Subscribe(ErrorHandler).AddTo(disposables);
request.OnSuccess.Subscribe(SuccessHandler).AddTo(disposables);
```

request.Send();


### Example 2, Async:
```
var request = APIRequest.Get("URL");

await request.SendAsync();

if (request.requestState == EResponseState.success)
{
	var result = request.response.DataAsText;
	//Handle success case
}
else if (request.requestState == EResponseState.error)
{
	//Handle error case
}
```

## Reasoning

This class was created to interact with specific API, that returns JSON container. This container always have several fields, that indicate status of request, as well as data.

User can deal with a response in one place in both success and fail situations.

In order to handle authorication, you can create request with token, that will used in header as Bearer token:
```
var request = APIRequest.Get("URL", "token");
```

To limit errors, separate different calls and simplify arguments maagment, user can create static class, that will handle generating requests:

```
public static class RequestsGroup
{

	public APIRequest RequestOne()
	{
		return APIRequest.Get($"{api.Path}endpoint1", api.token);
	}

	public APIRequest RequestTwo(string argument)
	{
		return APIRequest.Get($"{api.Path}endpoint2?argument={argument}", api.token);
	}
	
	public APIRequest RequestThree(string argument)
	{
		var request = APIRequest.Get($"{api.Path}endpoint3", api.token);
		
		var dataAsString = JsonConvert.SerializeObject(
                new DataModelClass() { field = argument });
            var data = Encoding.UTF8.GetBytes(dataAsString);

            request.RawData = data;
			
		return request;
	}
}
```

Then, instead of creating request from APIRequest, user will do:

```

var request1 = RequestsGroup.RequestOne();
var request2 = RequestsGroup.RequestTwo("argument");

```


## SendAsync<T>
SendAsync<T>() will converte content of response to given class T.

```
class DataModel 
{
	public string data;
}

var request = RequestsGroup.RequestOne();

var response = await request.SendAsync<DataModel>();

switch (request.responseState)
{
	case EResponseState.succes:
		//Handle data
		Debug.Log(response.data);
		break;

	case EResponseState.error:
		//Handle error
		break;

	case EResponseState.noResponse:
		//Handle no response
		break;
}

```


## Json

SendAsyncReturnJson() will converte a response result to Json.


## Possible improvments

APIRequest generate new object for every request, and in case of massive amount of requests, can lead to memory leak. Object Pool can be used to improve this area.

